#!/usr/bin/env python3
# -*- coding: utf-8 -*-ail


from tkinter import *
import cv2
import numpy as np
from tkinter.ttk import Style
from tkinter import messagebox
from baseDatosProyectoLatas import*
import matplotlib.pyplot as plt
from PIL import Image, ImageTk as itk
from procMedicion import procedMedicion
from medicionesMilimetros import medicionAltura, medicionCubeta, medicionEspesor, controlDeMedidas

#from medicionesMilimetros import imgBordesAltura,imgBordesEspesor,imgBordesCubeta

#----------------------Funciones---------------
ubicacion1 = (190,35)
ubicacion2 = (190,190)
ubicacion3 = (190,360)
ubicacion4 = (500,100)
estadoInicio = False
colorFondoCuadroMedias = "#ff0f0f" 

def levantarImagenDeEstado():
    global imgOk1
    global imgOk2
    global imgOk3
    global imgBad1
    global imgBad2
    global imgBad3
    global imgAprob
    global imgRecha
#----------- Imagenes de estados (bien/mal) --------------------
    imgOk1 = Image.open('bien1.jpg')
    imgOk1 = imgOk1.resize((100,100), Image.ANTIALIAS)
    imgOk1 = itk.PhotoImage(imgOk1)
    imgOk2=imgOk1
    imgOk3=imgOk1

    imgBad1 = Image.open('mal1.jpg')
    imgBad1 = imgBad1.resize((100,100), Image.ANTIALIAS)
    imgBad1 = itk.PhotoImage(imgBad1)
    imgBad2=imgBad1
    imgBad3=imgBad1
    
    imgAprob = Image.open('aprobado.jpg')
    imgAprob = imgAprob.resize((250,250), Image.ANTIALIAS)
    imgAprob = itk.PhotoImage(imgAprob)
    
    imgRecha = Image.open('rechazado.jpg')
    imgRecha = imgRecha.resize((250,250), Image.ANTIALIAS)
    imgRecha = itk.PhotoImage(imgRecha)

    #return imgOk1,imgOk2,imgOk3,imgBad1,imgBad2,imgBad3,imgAprob,imgRecha

def salirAplicacion(root):
    valor = messagebox.askquestion("Salir","¿Desea salir de la aplicacion?")
    if valor == "yes":
        root.destroy()

def medicion(numeroDisplay,frameMedida):       
   
    global cont
    if (numeroDisplay.get()=="0"):
        cont = 0
        levantarImagenDeEstado()
        
    global estadoInicio
    estadoInicio = True
    cont+=1
    numeroDisplay.set(cont)
    (rutaAltura,rutaEspesor,rutaCubeta) = procedMedicion() #Se toman las 3 imagenes, se guardan y se obtiene las rutas
    mmAltura = round(medicionAltura(rutaAltura),2)
    mmCubeta = round(medicionCubeta(rutaCubeta),2)
    mmEspesor = round(medicionEspesor(rutaEspesor),2)
    (estadoAltura,estadoEspesor,estadoCubeta) = controlDeMedidas(mmAltura,mmEspesor,mmCubeta)
    cargaDeImagen(rutaAltura,rutaEspesor,rutaCubeta)
    estado = refrescoDeImagen(frameMedida,estadoAltura,estadoEspesor,estadoCubeta)
    insertMedidasMilimetros(frameMedida,mmEspesor,mmAltura,mmCubeta)
    crearRegistro(mmAltura,mmEspesor,mmCubeta,estado,rutaAltura,rutaEspesor,rutaCubeta)
        
def cargaDeImagen(rutaAltura,rutaEspesor,rutaCubeta):
    global fotoEspesor
    global fotoAltura
    global fotoCubeta
    global imgBBDDEspesor
    global imgBBDDAltura
    global imgBBDDCubeta
    
    
#    cv2.imwrite('BordesEspesor.jpg',imgBordesEspesor(rutaEspesor)) 
#    cv2.imwrite('BordesAltura.jpg',imgBordesAltura(rutaAltura)) 
#    cv2.imwrite('BordesCubeta.jpg',imgBordesCubeta(rutaCubeta)) 
    
    rutaBordesAltura='/home/pi/Desktop/MedicionSuperSopa/BordesAltura.jpg'
    rutaBordesEspesor='/home/pi/Desktop/MedicionSuperSopa/BordesEspesor.jpg'
    rutaBordesCubeta='/home/pi/Desktop/MedicionSuperSopa/BordesCubeta.jpg'
    
#    imgBBDDEspesor = cv2.imread(rutaEspesor,0)
#    imgEspesor = Image.open(rutaEspesor)
    imgBBDDEspesor = cv2.imread(rutaBordesEspesor,0)
    imgEspesor = Image.open(rutaBordesEspesor)
 ###################################################   
    
    imgEspesor = imgEspesor.resize((150,150), Image.ANTIALIAS)
    fotoEspesor = itk.PhotoImage(imgEspesor)
    
#    imgBBDDAltura = cv2.imread(rutaAltura,0)
#    imgAltura = Image.open(rutaAltura)
   
    imgBBDDAltura = cv2.imread(rutaBordesAltura,0)
    imgAltura = Image.open(rutaBordesAltura)
#############################################    
    imgAltura = imgAltura.resize((150,150), Image.ANTIALIAS)
  
    fotoAltura = itk.PhotoImage(imgAltura)
    
#    imgBBDDCubeta = cv2.imread(rutaCubeta,0)
#    imgCubeta = Image.open(rutaCubeta)
    
    imgBBDDCubeta = cv2.imread(rutaBordesCubeta,0)
    imgCubeta = Image.open(rutaBordesCubeta)   
    
#######################################  
    imgCubeta = imgCubeta.resize((150,150), Image.ANTIALIAS)
   
    fotoCubeta = itk.PhotoImage(imgCubeta)
    
    #return imgBBDDEspesor,imgBBDDAltura,imgBBDDCubeta,fotoEspesor,fotoAltura,fotoCubeta

def refrescoDeImagen(frameMedida,estadoAltura,estadoEspesor,estadoCubeta):

    if estadoInicio: 
        
        labelEspesor=Label(frameMedida, image=fotoEspesor)
        labelEspesor.place(x=20,y=10)   
                
        
        labelCubeta=Label(frameMedida, image=fotoCubeta)
        labelCubeta.place(x=20,y=170)
       
        labelAltura=Label(frameMedida, image=fotoAltura)
        labelAltura.place(x=20,y=330)     
    
       
        if estadoEspesor:
            labelOk=Label(frameMedida, image=imgOk1)
            labelOk.place(x=ubicacion1[0],y=ubicacion1[1])
        else:
            labelBad =Label(frameMedida, image=imgBad1)
            labelBad.place(x=ubicacion1[0],y=ubicacion1[1])  
        
        if estadoAltura:
            labelOk=Label(frameMedida, image=imgOk2)
            labelOk.place(x=ubicacion3[0],y=ubicacion3[1])
        else:
            labelBad =Label(frameMedida, image=imgBad2)
            labelBad.place(x=ubicacion3[0],y=ubicacion3[1])  
        
        if estadoCubeta:
            labelOk=Label(frameMedida, image=imgOk3)
            labelOk.place(x=ubicacion2[0],y=ubicacion2[1])
        else:
            labelBad =Label(frameMedida, image=imgBad3)
            labelBad.place(x=ubicacion2[0],y=ubicacion2[1])  
        
        if estadoEspesor and estadoAltura and estadoCubeta:
            labelAprob=Label(frameMedida,image=imgAprob)
            labelAprob.place(x=ubicacion4[0],y=ubicacion4[1])
            estado = "OK"
            return estado
        else:
            labelRecha=Label(frameMedida,image=imgRecha)
            labelRecha.place(x=ubicacion4[0],y=ubicacion4[1])
            estado = "Rechazado"
            return estado

def insertMedidasMilimetros(frameMedida,mmEspesor,mmAltura,mmCubeta):
    mm_Espesor = StringVar()
    mm_Altura = StringVar()
    mm_Cubeta = StringVar()
    mm_Espesor.set(mmEspesor)
    mm_Altura.set(mmAltura)
    mm_Cubeta.set(mmCubeta)
    displayEspesor = Entry(frameMedida, textvariable=str(mm_Espesor))
    displayEspesor.place(x=300,y=50, width=100, height=20)
    displayEspesor.config(bg="white",fg="black", justify="center")
    
    displayCubeta = Entry(frameMedida, textvariable=str(mm_Cubeta))
    displayCubeta.place(x=300,y=205, width=100, height=20)
    displayCubeta.config(bg="white",fg="black", justify="center")
    
    displayAltura = Entry(frameMedida, textvariable=str(mm_Altura))
    displayAltura.place(x=300,y=360, width=100, height=20)
    displayAltura.config(bg="white",fg="black", justify="center")
    
    mm1 = Label(frameMedida,text="mm")
    mm2 = Label(frameMedida,text="mm")
    mm3 = Label(frameMedida,text="mm")
    mm1.place(x=405,y=50, width=40, height=20)
    mm1.config(bg = colorFondoCuadroMedias)
    mm2.place(x=405,y=205, width=40, height=20)
    mm2.config(bg = colorFondoCuadroMedias)
    mm3.place(x=405,y=360, width=40, height=20)
    mm3.config(bg = colorFondoCuadroMedias)

def refrescarBase(listbox):
    listbox.delete(0,END)
    listaMediciones = leerRegistro()
    for row in listaMediciones:
        listbox.insert(END, row)

def borrarID(id_Medicion,cuadroTexto):
    id_Medicion = cuadroTexto.get()
    borrarRegistro(id_Medicion)
    refrescarBase()

