"""
Created on Fri Jul 19 20:12:25 2019

@author: pi
"""
#configuracion
import RPi.GPIO as gp
import os
import cv2
import numpy as np
import datetime
#obtener la biblioteca de tiempo
import time

gp.setwarnings(False)
gp.setmode(gp.BOARD)

#eliminando los warings
gp.setwarnings(False)
pins = [40,38,36,37] 
gp.setup(pins, gp.OUT, initial=1)

gp.setup(7, gp.OUT)
gp.setup(11, gp.OUT)
gp.setup(12, gp.OUT)

gp.setup(15, gp.OUT)
gp.setup(16, gp.OUT)
gp.setup(21, gp.OUT)
gp.setup(22, gp.OUT)

gp.output(11, True)
gp.output(12, True)
gp.output(15, True)
gp.output(16, True)
gp.output(21, True)
gp.output(22, True)

#%% Definiciones de funciones

switchCam = {
        1 : " -rot 0 -o imagen_",
        3 : " -rot  180 -o imagen_",
        4 : " -rot 0 -o imagen_"
        }

#rapiStr = "raspistill -t 000  -q 100"
rapiStr = "raspistill -t 1000 -w 1024 -h 600 -q 100"

def capture(cam):
    x = datetime.datetime.now().isoformat()
    cmd = rapiStr + switchCam[cam] + str(x) + ".jpeg"        
    #print(cmd)
    os.system(cmd)
    ruta = "imagen_" + str(x) + ".jpeg"
    return ruta

# Medicion de la Altura del cierre
def cap_altura():
    gp.output(7, True)
    gp.output(11, True)
    gp.output(12, False)
    gp.output(38, gp.LOW) #38   
    ruta = capture(4)#altura   #rele nro 1    
    time.sleep(1)# tiempo en segundos
    gp.output(38, gp.HIGH)#38
    #binarizado(4)
    return ruta
    
# Medicion de la profundidad de cubeta
def cap_prof():

    gp.output(7, False)
    gp.output(11, False)
    gp.output(12, True)   
    gp.output(37,  gp.LOW)  #37 
#    gp.output(38,  gp.LOW)#38 - Led circular inferior
    ruta = capture(1)#prof cubeta    #rele nro  3
    time.sleep(1)
    gp.output(37,  gp.HIGH)#37
    gp.output(38,  gp.HIGH)#38 - Led circular inferior
    #binarizado(1)
    return ruta

# Medicion del ancho de cierre
def cap_cierre():

    gp.output(7, False)
    gp.output(11, True)
    gp.output(12, False)
    gp.output(36,  gp.LOW)#36
    gp.output(40,  gp.LOW)#40
    ruta = capture(3)#ancho   #rele nro 2
    time.sleep(1)
    gp.output(36,  gp.HIGH)#36
    gp.output(40,  gp.HIGH)#40
    #binarizado(3)
    return ruta

def binarizado(cam):
    img1 = cv2.imread('/home/pi/Code/medicion/imagen_%d.jpg'%cam,0)
    ret,mascara = cv2.threshold(img1,0,255,cv2.THRESH_OTSU)#binarizo
    cv2.imshow('imagen_bin_%d'%cam,mascara)
    np.save('imagen_bin_%d'%cam,mascara)


def procedMedicion():
    rutaEspesor = cap_cierre()  # patron 8x8 
    rutaCubeta = cap_prof() # PATRON 25X7
    rutaAltura = cap_altura() #
    return rutaAltura,rutaEspesor,rutaCubeta

#def procedMedicion():
#    rutaEspesor = 'espesor.jpg'
#    rutaCubeta = 'cubeta.jpg'
#    rutaAltura = 'altura.jpg'
#    return rutaAltura,rutaEspesor,rutaCubeta

##%% 
#
#def main():
#     rutaEspesor = cap_cierre()  # patron 8x8 
#     rutaCubeta = cap_prof() # PATRON 25X7
#     rutaAltura = cap_altura() #
#
#    
#if __name__ == "__main__":
#    main()
#
#    gp.output(7, False)
#    gp.output(11, False)
#    gp.output(12, True)
#    
    