#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 21 21:00:03 2019

@author: pi
"""

import cv2
import matplotlib.pyplot as plt
import numpy as np
import latas
from scipy.optimize import minimize
from scipy import ndimage, cluster
from skimage.morphology import opening, disk
import cilindrImageLib as cilib


def controlDeMedidas(mmAltura,mmEspesor,mmCubeta):
    limMaxAltura = 3.35
    limMinAltura = 2.9
    limMaxEspesor = 1.5
    limMinEspesor = 0.9
    limMaxCubeta = 3.9
    limMinCubeta = 3.1
    if mmAltura < limMaxAltura and mmAltura >limMinAltura:
        estadoAltura = True
    else:
        estadoAltura = False
    if mmEspesor < limMaxEspesor and mmEspesor >limMinEspesor:
        estadoEspesor = True
    else:
        estadoEspesor = False
    if mmCubeta < limMaxCubeta and mmCubeta >limMinCubeta:
        estadoCubeta = True
    else:
        estadoCubeta = False   
    return estadoAltura,estadoEspesor,estadoCubeta

#ALTURA#################################################################3

def costoAltura(x,xdatoI,ydatoI,xdatoS,ydatoS):
        
        bs= x[0]*xdatoS[0]+x[2]-ydatoS[1]
        
        bi= x[0]*xdatoI[0]+x[1]-ydatoI[1]
        
        return np.sum(bs**2) + np.sum(bi**2) 
    
def medicionAltura(rutaAltura):
    #imgAltura=cv2.imread('imagen_2019-11-12T06:52:46.565800.jpg')
    
    #imgAltura=cv2.imread('imagen_2019-11-12T06:55:54.411462.jpg')
    
    #imgAltura=cv2.imread('/home/pi/Code/medicion/imagen_2019-12-16T22:49:00.091577.jpg')
    imgAltura=cv2.imread(rutaAltura)
    imagenSalida,bordeInf,bordeSup,u=latas.BordesAltura(imgAltura)
    
    s0_Sup, s0_Inf, h0_Sup, h0_Inf=latas.altura2mm(imgAltura,bordeInf,bordeSup)
    
    alturaV=h0_Sup-h0_Inf
    
    alturaM=np.mean(alturaV)
    
    datosI=np.array([np.reshape(s0_Inf,(-1,1)),np.reshape(h0_Inf,(-1,1))])#datos borde inferior
        
    datosS=np.array([np.reshape(s0_Sup,(-1,1)),np.reshape(h0_Sup,(-1,1)) ])#datos borde superior
      
        
    
    a=-0.01
    bI=-17.83
    bS=-14.61
    
    rRr=(a,bI,bS)
        
    costoAltura(rRr,datosI[0],datosI[1],datosS[0],datosS[1])    
        #Se minimiza el error cuadratico de la funcion costo.
    
    pars = minimize(costoAltura,rRr,args=(datosI[0],datosI[1],datosS[0],datosS[1]) , method='Powell')
    
    param=pars.x
    
    yInf=param[0]*datosI[0]+param[1]
    ySup=param[0]*datosS[0]+param[2]
    
    alfa=np.arctan(param[0])
    
    dy=abs(param[2]-param[1])
    d=dy*np.cos(alfa)
    
    print('medicion altura [mm]: '+ str(dy) + ' mm') 
    #plotearGraficosAltura(u,bordeSup,bordeInf,s0_Sup, h0_Sup,s0_Inf, h0_Inf,d,imagenSalida)
    imgConScater = crearImagenBordes(bordeSup,bordeInf,imagenSalida)
    cv2.imwrite('BordesAltura.jpg',imgConScater)
    return dy



def plotearGraficosAltura(u,bordeSup,bordeInf,s0_Sup, h0_Sup,s0_Inf, h0_Inf,d,imagenSalida):    
    
    plt.figure()
    ax1 = plt.subplot(2,2,3)
    ax2 = plt.subplot(2,2,4)
    
    ax1.scatter(u, (bordeSup), c='g', marker='+')
    ax1.scatter(u, (bordeInf), c='r', marker='+')
    ax1.set_xlabel('u [px]')
    ax1.set_ylabel('v [px]')
    ax1.axis('equal')
    plt.title('Altura de cierre: '+ str((d)) + ' mm')
    
    ax2.scatter(s0_Sup, h0_Sup, c='g', marker='+')
    ax2.scatter(s0_Inf, h0_Inf, c='r', marker='+')
    ax2.set_xlabel('s [mm]')
    ax2.set_ylabel('h [mm]')
    ax2.axis('equal')
    
    ax3 = plt.subplot(2,2,1)
    ax3.imshow(imagenSalida)
    ax3.plot(bordeSup,'g',lw=2.0)
    ax3.plot(bordeInf,'r',lw=2.0)
    
# ESPESOR  #############################################
def costoEspesor(x,xdato1,ydato1,xdato2,ydato2):
    cS=np.sqrt((xdato2-x[0])**2  + (ydato2-x[1])**2) - x[3]
    
    cIN=np.sqrt((xdato1-x[0])**2  + (ydato1-x[1])**2) - x[2]
            
    return np.sum(cS**2) + np.sum(cIN**2)

def medicionEspesor(rutaEspesor):
    imgEspesor=cv2.imread(rutaEspesor)
    maskEspesor=cv2.imread('maskespesor.jpg',0)
    
    imagenSalida,bordeSup,bordeInf=latas.BordesEspesor(imgEspesor,maskEspesor)
    
    u = np.reshape(np.arange(len(bordeSup)),(-1,))
    
    (dst1,dst2)=latas.espesor2mm(bordeSup,bordeInf)
    
    
    datosS=np.array([np.reshape(dst1.T[0],(-1,1)),np.reshape(dst1.T[1],(-1,1))])#datos borde inferior
        
    datosI=np.array([np.reshape(dst2.T[0],(-1,1)),np.reshape(dst2.T[1],(-1,1)) ])#datos borde superior
    
    rRr=( 10,- 20,23.9 ,25)
        
        
        #Se minimiza el error cuadratico de la funcion costo.
    #    resR =  minimize(costo,rRr,args=(datosI[0],datosI[1],datosS[0],datosS[1]) , method='Powell'    )
    pars =  minimize(costoEspesor,rRr,args=(datosI[0],datosI[1],datosS[0],datosS[1]) , method='Powell')
    
    rR=pars.x
    
    #print(rR)
    
    espesor=np.sqrt( rR[3]-rR[2] )
    
    print('medicion espesor [mm]: '+ str((espesor)) + ' mm')   
    #plotearGraficosEspesor(u,bordeSup,bordeInf,espesor,dst1,dst2,imagenSalida)
    imgConScater = crearImagenBordes(bordeSup,bordeInf,imagenSalida)
    cv2.imwrite('BordesEspesor.jpg',imgConScater)
    return espesor
    
def plotearGraficosEspesor(u,bordeSup,bordeInf,espesor,dst1,dst2,imagenSalida):    
    plt.figure()
    ax1 = plt.subplot(2,2,3)
    ax2 = plt.subplot(2,2,4)
    
    ax1.scatter(u, (bordeSup), c='b', marker='+')
    ax1.scatter(u, (bordeInf), c='r', marker='+')
    ax1.set_xlabel('u [px]')
    ax1.set_ylabel('v [px]')
    ax1.axis('equal')
    plt.title('Espesor de cierre:   ' + str(np.float16(espesor)) + ' mm')
    
    ax2.scatter(dst1.T[0], dst1.T[1], c='b', marker='+')#sup
    ax2.scatter(dst2.T[0], dst2.T[1], c='r', marker='+')#inf
    ax2.set_xlabel('s [mm]')
    ax2.set_ylabel('h [mm]')
    ax2.axis('equal')
    
    ax3 = plt.subplot(2,2,1)
    ax3.imshow(imagenSalida)
    ax3.plot(bordeSup,'b',lw=2.0)
    ax3.plot(bordeInf,'r',lw=2.0)
    
# CUBETA ##############################################
def costoCubeta(x,xdatoI,ydatoI,xdatoS,ydatoS):
    bs= x[0]*xdatoS[0]+x[2]-ydatoS[1]
    
    bi= x[0]*xdatoI[0]+x[1]-ydatoI[1]

    return np.sum(bs**2) + np.sum(bi**2) 

def medicionCubeta(rutaCubeta):

    imgCubeta=cv2.imread(rutaCubeta)
    
    maskCubeta=cv2.imread('maskcubeta.jpeg',0)
    
    imgSalida,bordeSup,bordeInf= latas.BordesProfCubeta(imgCubeta,maskCubeta)
    
    s0_Sup, s0_Inf, h0_Sup, h0_Inf=latas.cubeta2mm(imgCubeta,imgSalida,bordeSup,bordeInf)
    
    u = np.reshape(np.arange(900),(-1,))
    
    
    
    
    datosI=np.array([np.reshape(s0_Inf,(-1,1)),np.reshape(h0_Inf,(-1,1))])#datos borde inferior
        
    datosS=np.array([np.reshape(s0_Sup,(-1,1)),np.reshape(h0_Sup,(-1,1)) ])#datos borde superior
        
        
    
    a=-0.005
    bI=1.45
    bS=-1.4
    
    rRr=(a,bI,bS)
        
    costoCubeta(rRr,datosI[0],datosI[1],datosS[0],datosS[1])    
        #Se minimiza el error cuadratico de la funcion costo.
    #    resR =  minimize(costo,rRr,args=(datosI[0],datosI[1],datosS[0],datosS[1]) , method='Powell'    )
    parsC= minimize(costoCubeta,rRr,args=(datosI[0],datosI[1],datosS[0],datosS[1]) , method='Powell')
    
    paramC=parsC.x
    
    alfaC=np.arctan(paramC[0])
    
    dyC=abs(paramC[2]-paramC[1])
    
    profCubeta=dyC*np.cos(alfaC)
    
    print('medicion profCubeta [mm]: '+ str(profCubeta) + ' mm') 
   # plotearGraficosCubeta(u,bordeSup,bordeInf,profCubeta,s0_Sup,s0_Inf,h0_Sup,h0_Inf,imgSalida)
    imgConScater = crearImagenBordes(bordeSup,bordeInf,imgSalida)
    cv2.imwrite('BordesCubeta.jpg',imgConScater)
    
    return profCubeta

def plotearGraficosCubeta(u,bordeSup,bordeInf,profCubeta,s0_Sup,s0_Inf,h0_Sup,h0_Inf,imgSalida):    
    plt.figure()
    ax1 = plt.subplot(2,2,3)
    ax2 = plt.subplot(2,2,4)
    
    ax1.scatter(u, (bordeSup), c='b', marker='+')
    ax1.scatter(u, (bordeInf), c='r', marker='+')
    ax1.set_xlabel('u [px]')
    ax1.set_ylabel('v [px]')
    ax1.axis('equal')
    plt.title('Profundidad de cubeta: '+str(np.float16(profCubeta))+ ' mm')
    ax2.scatter(s0_Sup, h0_Sup, c='b', marker='+')
    ax2.scatter(s0_Inf, h0_Inf, c='r', marker='+')
    ax2.set_xlabel('s [mm]')
    ax2.set_ylabel('h [mm]')
    ax2.axis('equal')
    
    ax3 = plt.subplot(2,2,1)
    ax3.imshow(imgSalida)
    ax3.plot(bordeSup,'b',lw=2.0)
    ax3.plot(bordeInf,'r',lw=2.0)
    
    #################BORDES######################
    
    
def crearImagenBordes(bordeSup,bordeInf,imgSalida):    
    a = np.arange(len(bordeSup))
    copia = np.copy(imgSalida)
    [cv2.drawMarker(copia,(a[i],bordeSup[i]),(255,0,0),cv2.MARKER_SQUARE,3,1) for i in range(len(a)) ]
    b = np.arange(len(bordeInf))
    [cv2.drawMarker(copia,(a[j],bordeInf[j]),(255,0,0),cv2.MARKER_SQUARE,3,1,cv2.LINE_8) for j in range(len(b)) ]
    return copia

    
#    
#def borde_altura(rutaAltura):
#   
# imgAltura=cv2.imread(rutaAltura)
#
# imagenSalida,bordeInf,bordeSup,u=latas.BordesAltura(imgAltura)
#
# return imagenSalida,bordeInf,bordeSup,u
#    
#
#def borde_espesor(rutaEspesor):
#    
# imgEspesor=cv2.imread(rutaEspesor)
# maskEspesor=cv2.imread('maskespesor.jpg',0)
# imagenSalida,bordeSup,bordeInf=latas.BordesEspesor(imgEspesor,maskEspesor)
# u = np.reshape(np.arange(latas.xmaxE-latas.xminE)+latas.xminE,(-1,)) 
# return imagenSalida,bordeInf,bordeSup,u
#  
#def borde_cubeta(rutaCubeta):
#          
#  imgCubeta=cv2.imread(rutaCubeta)
#  maskCubeta=cv2.imread('maskcubeta.jpeg',0)
#  imagenSalida,bordeSup,bordeInf= latas.BordesProfCubeta(imgCubeta,maskCubeta)
#  u = np.reshape(np.arange(latas.xmaxC-latas.xminC)+latas.xminC,(-1,)) 
#
#  return imagenSalida,bordeInf,bordeSup,u
#
##def imgBordesAltura(imagenSalida,bordeInf,bordeSup,u):
#    
#
#
#def imgBordesAltura(rutaAltura):  
#    
#    bordesA=borde_altura(rutaAltura)  
#    
#    ptsInf=np.array([bordesA[3].T,bordesA[1].T]).T.reshape(-1,1,2)
#    ptsSup=np.array([bordesA[3].T,bordesA[2].T]).T.reshape(-1,1,2)
#    
#    imgOut1=cv2.polylines(bordesA[0],ptsInf,True,(0,255,0),2,cv2.LINE_8,0)
#    
#    imgOut2=cv2.polylines(imgOut1,ptsSup,True,(255,0,0),2,cv2.LINE_8,0)
#    
#    
#    return imgOut2
#     
#
#def imgBordesEspesor(rutaEspesor):
#    
#    bordesE=borde_espesor(rutaEspesor)
#    
#    ptsInf=np.array([bordesE[3].T,bordesE[1].T]).T.reshape(-1,1,2)
#    ptsSup=np.array([bordesE[3].T,bordesE[2].T]).T.reshape(-1,1,2)
#    
#    imgOut1=cv2.polylines(bordesE[0],ptsInf,True,(0,255,0),2,cv2.LINE_8,0)
#    
#    imgOut2=cv2.polylines(imgOut1,ptsSup,True,(255,0,0),2,cv2.LINE_8,0)
#    
#    
#    return imgOut2
#
#
#
#def imgBordesCubeta(rutaCubeta):
#
#    bordesC=borde_cubeta(rutaCubeta) 
#     
#    ptsInf=np.array([bordesC[3].T,bordesC[1].T]).T.reshape(-1,1,2)
#    ptsSup=np.array([bordesC[3].T,bordesC[2].T]).T.reshape(-1,1,2)
#    
#    imgOut1=cv2.polylines(bordesC[0],ptsInf,True,(0,255,0),2,cv2.LINE_8,0)
#    
#    imgOut2=cv2.polylines(imgOut1,ptsSup,True,(255,0,0),2,cv2.LINE_8,0)
#    
#    
#    return imgOut2    
#    
    






