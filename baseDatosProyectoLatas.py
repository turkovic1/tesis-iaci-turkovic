import sqlite3
from tkinter import messagebox
import os
import csv
def conexionBBDD():
    conexionBBDD = sqlite3.connect("baseDeDatos.sqlite3")
    puntero = conexionBBDD.cursor()
    try:
        # comenta las siguientes lineas, porque la tabla ya esta creada.
        puntero.execute(''' CREATE TABLE TABLA_MEDICIONES (
                id_Medicion INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                mm_altura VARCHAR(50),
                mm_espesor VARCHAR(50),
                mm_cubeta VARCHAR(50)
                estado VARCHAR(10),
                ruta_altura VARCHAR(100),
                ruta_espesor VARCHAR(100),
                ruta_cubeta VARCHAR(100))''')
        messagebox.showinfo("BBDD","Base de Dato creada con éxito")
    except:
        messagebox.showwarning("Atencion!!", "La Base de Dato ya existe")
    
    
    puntero.close()

    #Guardamos los cambios en la base de datos
    conexionBBDD.commit()
    #cerramos la conexion a la base de datos

    conexionBBDD.close()
    
def crearRegistro(mmAltura,mmEspesor,mmCubeta,estado,rutaAltura,rutaEspesor,rutaCubeta):
    argumentos = mmAltura,mmEspesor,mmCubeta,estado,rutaAltura,rutaEspesor,rutaCubeta
    conexionBBDD = sqlite3.connect("baseDeDatos.sqlite3")
    puntero = conexionBBDD.cursor()
    sql = '''INSERT INTO TABLA_MEDICIONES(mm_altura, mm_espesor, mm_cubeta, estado, ruta_altura, ruta_espesor, ruta_cubeta) VALUES (?, ?, ?, ?, ?, ?, ?)'''
    #Realizar la consulta
    try:
        puntero.execute(sql,argumentos)
        messagebox.showinfo("BBDD","Registro ingresado con éxito")
    except:
        messagebox.showwarning("Atencion!!", "Hubo un error en el ingreso del registro")
    puntero.close()  
    #guardamos los cambios en la base de datos
    conexionBBDD.commit()
    #cerramos la conexion a la base de datos
    conexionBBDD.close()
    
def leerRegistro():
    conexionBBDD = sqlite3.connect("baseDeDatos.sqlite3")
    
    #Seleccionar el cursor para iniciar la consulta
    puntero = conexionBBDD.cursor()
    
    puntero.execute("SELECT * FROM TABLA_MEDICIONES")
    
    laMedicion = puntero.fetchall()
    
    puntero.close()  
    #guardamos los cambios en la base de datos
    conexionBBDD.commit()
    #cerramos la conexion a la base de datos
    conexionBBDD.close()
    
    return laMedicion

def actualizarRegistro(argumentos):
    conexionBBDD = sqlite3.connect("baseDeDatos.sqlite3")
    puntero = conexionBBDD.cursor()
    
    sql = '''UPDATE TABLA_MEDICIONES 
    SET mm_altura = ?,
        mm_espesor = ?
        mm_cubeta = ?,
        estado = ?,
        ruta_altura = ?,
        ruta_espesor = ?,
        ruta_cubeta = ?,
        WHERE id_Medicion= ?'''
     #Realizar la consulta
    try:
        puntero.execute(sql,argumentos)
        messagebox.showinfo("BBDD","Registro actualizado con éxito")
    except:
        messagebox.showwarning("Atencion!!", "Hubo un error en la actualizacion del registro")
    puntero.close()  
    #guardamos los cambios en la base de datos
    conexionBBDD.commit()
    #cerramos la conexion a la base de datos
    conexionBBDD.close() 
    
def borrarRegistro(idMedicion):
        
    #Establecer la conexion
    conexionBBDD = sqlite3.connect("baseDeDatos.sqlite3")
    
    #Seleccionar el cursor para iniciar la consulta
    puntero = conexionBBDD.cursor()
    
    try:
        puntero.execute("DELETE FROM TABLA_MEDICIONES WHERE id_Medicion=" + idMedicion)  
        messagebox.showinfo("BBDD","Registro eliminado con éxito")
    except:
        messagebox.showwarning("Atencion!!", "Hubo un error en la eliminación del registro")
    
    puntero.close()  
    #guardamos los cambios en la base de datos
    conexionBBDD.commit()
    #cerramos la conexion a la base de datos
    conexionBBDD.close()

def exportar2Excel():
    conexionBBDD = sqlite3.connect("baseDeDatos.sqlite3")
    puntero = conexionBBDD.cursor()
    puntero.execute("SELECT * from TABLA_MEDICIONES")
    try:
        with open("TABLA_MEDICIONES.csv", "w") as out_csv_file:
           
            csv_out = csv.writer(out_csv_file)
            csv_out.writerow([d[0] for d in puntero.description])
#
            for result in puntero:
               csv_out.writerow(result)
        messagebox.showinfo("Tabla exportada exitosamente")
    except:
        messagebox.showwarning("Atencion!!", "Hubo un error al exportar la tabla")
    puntero.close()  
    #guardamos los cambios en la base de datos
    conexionBBDD.commit()
    #cerramos la conexion a la base de datos
    conexionBBDD.close()