#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 29 05:11:07 2019

@author: pi
"""
from scipy import ndimage, cluster
import numpy as np
import cv2
from skimage.morphology import opening, disk
import cilindrImageLib as cilib
from cv2 import getStructuringElement, MORPH_ELLIPSE, morphologyEx, MORPH_CLOSE

#%% FILTROS

def filtroAltura(imagenSinFIltrar):
        
   kernel=getStructuringElement(MORPH_ELLIPSE, (5,5))
   
   imagenFitrada1 = morphologyEx(imagenSinFIltrar, MORPH_CLOSE,kernel)  # CLOSING
   f1 = cv2.morphologyEx( imagenFitrada1 ,cv2.MORPH_OPEN,kernel) # OPENING
    
   return(f1)


def filtroEspesor(imagenSinFIltrar):
  
   kernel=cv2.getStructuringElement(cv2.MORPH_RECT, (3,3))
    
   f1 = cv2.morphologyEx(imagenSinFIltrar,cv2.MORPH_OPEN,kernel) # OPENING
   imagenFitrada = cv2.morphologyEx(f1,cv2.MORPH_CLOSE,kernel)  # CLOSING
    
   return(imagenFitrada )



def filtroCubeta(imagenSinFIltrar):
         
   kernel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3,3))
   
   f1 = cv2.morphologyEx(imagenSinFIltrar,cv2.MORPH_CLOSE,kernel) # OPENING
   imagenFitrada = cv2.morphologyEx(f1,cv2.MORPH_OPEN,kernel)  # CLOSING
    
   return(imagenFitrada )


def filtroCubeta2(imagenSinFIltrar):
         
   kernel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5,5))
   
   f1 = cv2.morphologyEx(imagenSinFIltrar,cv2.MORPH_CLOSE,kernel) # OPENING
   imagenFitrada = cv2.morphologyEx(f1,cv2.MORPH_OPEN,kernel)  # CLOSING
    
   return(imagenFitrada )

#%% deteccion de bordes.

global ymaxA
global yminA
global xmaxA
global xminA

    
ymaxA=450
yminA=335
xmaxA=900
xminA=350

def BordesAltura(imgAltura):
    img=np.copy(imgAltura[yminA:ymaxA,xminA:xmaxA])


#    img = img  
        
        #filtro bilateral
            
    d=-1
    sigmaColor=10
    sigmaSpace=10
        
    imagenF=cv2.bilateralFilter(img,d,sigmaColor,sigmaSpace)
        #######################################################
        
    imagenSalida=imagenF
        
        #######################################################
        #paso a escala de grises.
        
    imagenG=cv2.cvtColor(imagenSalida, cv2.COLOR_BGR2GRAY)
        
       
        #opening (en escala de grises).     
    selem = disk(5)
    imgOp = opening(imagenG, selem)
    
      
    # binarizado por kmeans
    maxG = np.max(imgOp)
    minG = np.min(imgOp)
    k_guess = np.array([minG, (minG + maxG) / 2, maxG])
    imgOpFlat = np.float32(np.reshape(imgOp, -1))
    centroides, clases = cluster.vq.kmeans2(imgOpFlat, k_guess)
  
    clases = np.reshape(clases, img.shape[:2])
    retKmea = (clases==0)*255
   
    imagenB=np.uint8(retKmea)    
  
    uu1=(imagenB)    

    ########################################

    pmerF3 = np.argmax(uu1, axis=0)
    
        
    bordeSup=  pmerF3

    u = np.reshape(np.arange(len(bordeSup)),(-1,))

#    maskr=np.uint8(np.zeros((uu1.shape[0]+2,uu1.shape[1]+2)))
#            
#    
#    point=(int((xmaxA-xminA)/2),int((ymaxA-yminA)-1))
#    
#         
#    newVal=255
#            
#    retval,Ar,maskR,rect=cv2.floodFill(uu1,maskr,point,newVal)
#        
    #imagenFlip1=Ar
    imagenFlip1=imagenB
    
    #imagenFlip=ndimage.rotate(imagenFlip1, 180, reshape=False)
    imagenFlip=imagenFlip1[::-1]
    
  #  imagenFlip[0] = imagenFlip[0]+255  
        
    bordeInf= np.argmax(imagenFlip, axis=0)#borde inferior
        
#    bordeInf=np.flip(bordeInf)
        
    bordeInf=ymaxA-yminA-bordeInf

    return(imagenSalida,bordeInf,bordeSup,u)
########################################################################
#%%
#MEDICION ESPESOR

global ymaxE
global yminE
global xmaxE
global xminE
 
ymaxE=250
yminE=50
xmaxE=900
xminE=100

def BordesEspesor(imgIN,maskIN):
    
    mask=np.copy(maskIN)
    img1=np.copy(imgIN)
    
     
    imagenSalida=img1[yminE:ymaxE,xminE:xmaxE]
    mask = mask[yminE:ymaxE,xminE:xmaxE]
  #  img1=cv2.cvtColor(imagenSalida, cv2.COLOR_BGR2GRAY)    
    
    img1=cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
    img1=img1[yminE:ymaxE,xminE:xmaxE]    
    
    ret1,maskB= cv2.threshold(mask,0,255,cv2.THRESH_OTSU)

    # binarizado por kmeans
    maxG = np.max(img1)
    minG = np.min(img1)
    k_guess = np.array([minG, (minG + maxG) / 2, maxG])
    imgOpFlat = np.float32(np.reshape(img1, -1))
    centroides, clases = cluster.vq.kmeans2(imgOpFlat, k_guess)  
    im1 = np.reshape(clases==0, img1.shape[:2])*255
    
    imagen1 = im1 + maskB
 
    #imagen0 = imagen1[yminE:ymaxE,xminE:xmaxE] 
    
    bordeSup = np.argmin(imagen1, axis=0) # borde superior
    
    
    im2c=imagen1
        
    ###########################################################################
    
            
    maskr = np.uint8(np.zeros((im2c.shape[0] + 2, im2c.shape[1] + 2)))
            
    point=(0, 199) 
         
    newVal = 255
            
    retval, Ar, maskR, rect = cv2.floodFill(im2c, maskr, point, newVal)
        
    imagenFlip1 = Ar
        
    imagenFlip = ndimage.rotate(imagenFlip1, 180, reshape=False)
    
    imagenFlip[0] = imagenFlip[0] + 255  
        
    bordeInf = np.argmin(imagenFlip, axis=0) # borde inferior
        
    bordeInf = np.flip(bordeInf)
        
    bordeInf = ymaxE - bordeInf - yminE
  
    
    return(imagenSalida,bordeSup,bordeInf)
  
    


#%% MEDICION DE PROFUNDIDAD DE CUBETA 

#ROI#cubeta##################################
    
global ymaxC
global yminC
global xmaxC
global xminC

    
ymaxC=400
yminC=100
xmaxC=1000
xminC=100
 #######################################  


def BordesProfCubeta(imgCubeta,maskCubeta):
    
#PREPROCESAMIENTO ######################################################
    mask=np.copy(maskCubeta)
    img=np.copy(imgCubeta)
   
        
    img=ndimage.rotate(img, 180, reshape=False)
    
    img4=img
    
    imgc=cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    
    ret1,imgh= cv2.threshold(imgc,0,255,cv2.THRESH_OTSU)

    mm=mask
   
    ret2,maskCubeta= cv2.threshold(mm,60,255,cv2.THRESH_BINARY)
    
    #filtrado###########################################
    
    kernel=cv2.getStructuringElement(cv2.MORPH_RECT, (7,7))
       
    img2s= cv2.morphologyEx(imgh,cv2.MORPH_OPEN,kernel)
    
    kernel=cv2.getStructuringElement(cv2.MORPH_RECT, (7,7))
       
    img2s= cv2.morphologyEx(imgh,cv2.MORPH_CLOSE,kernel)
    
    img2=filtroCubeta(imgh)
    ###################################################
    
    #BINARIZADO POR KMEANS
#    k_guess = np.array([30.0, 130.0, 250.0])
    imgOpFlat = np.float32(np.reshape(imgh, -1))

    maxG = np.max(imgOpFlat)
    minG = np.min(imgOpFlat)
    k_guess = np.array([minG, (minG + maxG) / 2, maxG])
    
    centroides, clases = cluster.vq.kmeans2(imgOpFlat, k_guess)  
    im1 = np.reshape(clases==0, imgh.shape[:2])*255
    

    im1=im1[0:285,xminC:xmaxC]
    
    BBS=ndimage.rotate(im1, 180, reshape=False)
    
    BBS[0]=BBS[0]+255
    bordeSup= np.argmin( BBS,axis=0)
        
    bordeSup=np.flip(bordeSup)
    
    ymax2=np.size(im1,0)
    
    bordeSup=ymax2-bordeSup - (ymaxC-ymax2)
     
######################################################################
    
    img2s=img2s[yminC:ymaxC,xminC:xmaxC]
    img2=img2[yminC:ymaxC,xminC:xmaxC]
        
    imgSalida=img4[yminC:ymaxC,xminC:xmaxC]
    
    mask=maskCubeta[yminC:ymaxC,xminC:xmaxC]
    
    ##################################################################
     # 1: borde inferior por encima de la luz 
     
#    bordeInf= np.argmax( img2,axis=0)
  ##################################################################
     # 2: borde inferior por abajo de la luz 
    
    BB=ndimage.rotate(img2, 180, reshape=False)
    
    bordeInf= np.argmax( BB,axis=0)
    
    
        
    bordeInf=np.flip(bordeInf)
    
    ymax2=np.size(img2,0)
    
    bordeInf=ymax2-bordeInf
   
    ##################################################################
  
    point=(450,285) 
         
    newVal=255
    maskr=np.uint8(np.zeros((img2s.shape[0]+2,img2s.shape[1]+2)))  
          
    retval,Ar,maskR,rect=cv2.floodFill(img2s,maskr,point,newVal)
        
    imagenFlip1=Ar
    
    maskr2=np.uint8(np.zeros((img2s.shape[0]+2,img2s.shape[1]+2)))      
    point2=(400,285) 
         
    newVal2=0
            
    retval,Ar2,maskR,rect=cv2.floodFill(Ar,maskr2,point2,newVal2)
        
    imagenFlip1=Ar2
        
    imagenFlip2=ndimage.rotate(imagenFlip1, 180, reshape=False)
    
    bordeSup=np.argmax(imagenFlip2,axis=0)#borde de la mascara
   
    bordeSup=np.flip(bordeSup)
    
    ymax2=np.size(imagenFlip2,0)
    
    bordeSup=ymax2-bordeSup
     

    return (imgSalida,bordeSup,bordeInf)

#%%

#FUNCIONES QUE MAPEAN BORDES EN PIXELES A BORDES EN MILIMETROS.

##############################################
global r0
r0 = 50 #radio de la lata


pars0=np.load('/home/pi/Code/Calibracion_Chessboard/pars0.npy')

parsAltura=np.load('/home/pi/Code/Calibracion_Chessboard/parsAltura.npy')
parScaAltura=np.load('/home/pi/Code/Calibracion_Chessboard/parScaAltura.npy')

homografia=np.load('/home/pi/Code/medicion/homografia.npy')

#(u, v, r, al, be, ga, tx, ty, tz, f, cx, cy):
parSca = np.array([1, 1, 1, 10, 10, 100, 1000])
########################################################
#%%

def altura2mm(imgAltura,bordeInf,bordeSup):

    imageSize = imgAltura.shape[:-1]
    
    cy0, cx0 = np.array(imageSize) / 2  #pto principal
    

   # imagenSalida,bordeInf,bordeSup,u=BordesAltura(imgAltura)
    
    u = np.reshape(np.arange(xmaxA-xminA)+xminA,(-1,)) # (posicion x [pix] de ambos bordes)
    
    vs = bordeSup+yminA #borde superior detectado  (posicion y [pix])
    
    fi0_Sup, h0_Sup = cilib.image2cylinder(u, vs, r0, *(parsAltura*parScaAltura), cx0, cy0)

    s0_Sup = fi0_Sup*r0
    
    vi = bordeInf +yminA #borde inferior detectado

    fi0_Inf, h0_Inf = cilib.image2cylinder(u, vi, r0, *(parsAltura*parScaAltura), cx0, cy0)
    s0_Inf = fi0_Inf*r0
    
    return (s0_Sup, s0_Inf, h0_Sup, h0_Inf)
  
#%%
def espesor2mm(bordeSup,bordeInf):


    #imagenSalida,bordeSup,bordeInf=BordesEspesor(imgEspesor,maskEspesor)
    
    vs = bordeSup+yminE #borde superior detectado  (posicion y [pix])
    
    u = np.reshape(np.arange(xmaxE-xminE)+xminE,(-1,)) # (posicion x [pix] de ambos bordes)
    
    vect1=np.array((u, vs) )
    
    srcPoints= np.float32(vect1.T).reshape((-1,1,2))
    
    dst1	= cv2.perspectiveTransform(srcPoints, homografia)
    
    
    vi = bordeInf +yminE #borde inferior detectado
    
    vect=np.array((u, vi) )
    
    srcPoints2= np.float32(vect.T).reshape((-1,1,2))
    
    dst2	= cv2.perspectiveTransform(srcPoints2, homografia)
    
    
    return (dst1,dst2)

#%%
def cubeta2mm(imgCubeta,imgSalida,bordeSup,bordeInf):
    
    imageSize = imgCubeta.shape[:-1]
    
    cy0, cx0 = np.array(imageSize) / 2  #pto principal
 
    
    #imgSalida,bordeSup,bordeInf= BordesProfCubeta(imgCubeta,maskCubeta)
    
    vs = bordeSup+yminC #borde superior detectado  (posicion y [pix])
    u = np.reshape(np.arange(len(vs)) + xminC, -1) # (posicion x [pix] de ambos bordes)
    
    fi0_Sup, h0_Sup = cilib.image2cylinder(u, vs, r0, *(pars0*parSca), cx0, cy0)

    s0_Sup = fi0_Sup*r0
    
    vi = bordeInf + yminC # borde inferior detectado

    fi0_Inf, h0_Inf = cilib.image2cylinder(u, vi, r0, *(pars0*parSca), cx0, cy0)
    s0_Inf = fi0_Inf*r0
    
    return (s0_Sup, s0_Inf, h0_Sup, h0_Inf)
    

