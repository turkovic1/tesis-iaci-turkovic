#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from funcUI import *

from tkinter import *
from tkinter.ttk import Style
from tkinter import messagebox
from PIL import Image, ImageTk as itk
import matplotlib.pyplot as plt
from baseDatosProyectoLatas import *
import numpy as np
import os


    
    
#---------------------- Definiciones ----------
root = Tk()
id_Medicion = ''
tamanoWindows = "800x600"
colorFondo = "#f00f0f"
colorLetra = "#000000"
colorFondoCuadroMedias = "#ff0f0f" 
colorFondoCuadroInicio = "#ffffff"


#------------------ Pestañas --------------------------------
notebook = ttk.Notebook(root)
notebook.pack(fill='both', expand='yes')
pes0 = ttk.Frame(notebook)
pes1 = ttk.Frame(notebook)
notebook.add(pes0, text='Medicion')
notebook.add(pes1, text='Lista de Mediciones')

#------------------ Pestaña Listado de Medicion -----------

listbox = Listbox(pes1)
listbox.place(x=10,y=10, width=750,height=400)
botonActualizar = Button(pes1 , text = "Actualizar Listado", command = lambda:refrescarBase(listbox))
botonActualizar.place(x=600, y=450, width=150, height=30)



textEliminarID = Label(pes1, text="Ingrese ID de la medicion que desea eliminar")
textEliminarID.place(x=10,y=450)

cuadroTexto = Entry(pes1, textvariable=id_Medicion)
cuadroTexto.place(x=10,y=470,width=50, height=30)

botonBorrar = Button(pes1 , text = "Borrar", command = lambda:borrarID(id_Medicion,cuadroTexto))
botonBorrar.place(x=70, y=470, width=100, height=30)

botonExportar = Button(pes1, text = "Exportar a Excel", command = lambda:exportar2Excel())
botonExportar.place(x=70, y=500, width=110, height=30)

botonCrearBBDD = Button(pes1, text="Crear Base de Dato",command=lambda:conexionBBDD())
botonCrearBBDD.place(x=600, y=415, width=150, height=30)
if os.path.isfile("baseDeDatos.sqlite3"):
    botonCrearBBDD.config(state = 'disabled')
else:
    botonCrearBBDD.config(state = 'normal')
#--------------Entorno grafico--------------------------------



root.title("MediVision") #titulo de la ventana
root.resizable(False,False) #se puede dimencionar alto y ancho de la ventana
#root.iconbitmap('@/home/pi/Desktop/MedicionSuperSopa/ico.bmp') #cargo el icono del vertice superior izquiero de la ventana

root.geometry(tamanoWindows) #Tamaño al inicio del programa
root.config(bg=colorFondo)

botonSalir = Button(root, text="Salir",command=lambda:salirAplicacion(root))
botonSalir.place(x=600, y=510, width=100, height=30)



               
#-------------- Cuadro de Inicio ------------------
frameInicio = Frame(pes0, width="800",height="100")
frameInicio.pack()

label = Label(frameInicio, text="Ingrese la lata y haga click en Medir")
label.pack()
 
botonMedir = Button(frameInicio, text="Medir", command=lambda:medicion(numeroDisplay,frameMedida), width=7).pack()

#------------- Frame de Medidas -------------------

frameMedida = Frame(pes0, width=800,height=500)
frameMedida.pack()
frameMedida.config(bg=colorFondoCuadroMedias)

#---------------- Contador de Eventos -------------
numeroDisplay = StringVar()
numeroDisplay.set(0)
display = Entry(pes0, textvariable=numeroDisplay)
display.place(x=600,y=10,width=100, height=20)
display.config(bg="black",fg="#03f943", justify="right")
#----------------- Base de Dato ------------------
text1 = Label(frameMedida,text="Espesor")
text2 = Label(frameMedida,text="Profundidad de Cubeta")
text3 = Label(frameMedida,text="Altura")
text1.place(x=300,y=30, width=100, height=20)
text1.config(bg = colorFondoCuadroMedias)
text2.place(x=300,y=185, width=150, height=20)
text2.config(bg = colorFondoCuadroMedias)
text3.place(x=300,y=340, width=100, height=20)
text3.config(bg = colorFondoCuadroMedias)

root.mainloop()

  
